# Parts selector
Parts_selector is a python utility to help you select optimal part's values for your electronic circuit.
## Parts series generation
You can use it to generate series of values of certain tollerance. e. g. resistors, capacitors,...
The formulas used to calculate those values are described [here](http://www.rfcafe.com/references/electrical/resistor-values.htm).
To generate values of parts which can be actualy bought you can use the function `stdVals()`
`stdVals(Int, tol = 10)`  takes 2 parameters.
1. `Int = [`<smallestValue>`, `<largestValue>`]`
    - `Int` is a 2 value array specifying an interval which will be searched for standard part values.
2. `tol =`<tolerance>
    - The second parameter: `tol` expresses the tolerance of standard part values in %. `tol` is an optional parameter and when not set, default value of 10 is used. It can, however, only be assigned one of these values: `[1, 2, 5, 10, 20]`
Function `stdVals` returns an array of values in the given interval of the specified tolerance. If none are found or if it encounters an error than empty array is returned.
### Examples:
1. Find part's values between 600 and 1000 with tolerance of 5%
    `rx = stdVals([600, 1000], 5)`
    The output is: `rx = [620.0, 680.0, 750.0, 830.0, 910.0]`
2. Find part's values between 600 and 1000 with tolerance of 1%
    `rx = stdVals([600, 1000], 1)`
    Output of which is: `rx = [604.0, 619.0, 634.0, 649.0, 665.0, 681.0, 698.0, 715.0, 732.0, 750.0, 768.0, 787.0, 806.0, 825.0, 845.0, 866.0, 887.0, 909.0, 931.0, 953.0, 976.0]`

3.  Find part's values between 600 and 1000 of 10% tolerance.
    `rx = stdVals([600, 1000])`
    The output is: `rx = [680.0, 830.0]`
To optimize values of your components in a certain application you simply need to generate values of the components to be potentially used using the method demonstrated in the previous paragraph. Then you change the mathematical expression defined as the function `func(val)`. Val is an array of n variables which represent the values you want to optimize.

## Optimization
Once you have defined all part's values start by creating an instance of the `Optimize` class. In it's constructor you can tweak its behaviour:
- `values` parameter is an array of arrays carrying the values to be optimized `values[0]` are the values of the first part and `values[n]` are the values of the n'th part.
- `yTarget` parameter is the return value of function `func(vals)` which the algorithm tries to get as close to as possible.
- `tol` parameter stands for acceptable deviation from `yTarget` defined in % 
- `epsilon` parameter stands for absolute acceptable deviation from `yTarget` 
- `units` is an array of names of measures and their units. These are used to display friendly results and therefore optional.
    - If you wish to use it you need to specify both name and unit of all arrays in `values` and the result of `func()` otherwise it won't work. eg. `units = [["Res", "Ohm"], ["Cap", "F"],["f", "Hz"]]` where `Res` is displayed with the first array in `values`, `Cap` analogically and `f` with the result of `func()`
- `printEnable` default = 1. If set to one the program will log output to console. Otherwise only calculated figures are returned.
- `sig` The number of significant digits to which output of `func()` is rounded.
Most parameters can only be set when calling the constructor. Only `yTarget` and `values` can be set later by calling member function `setyTarget` or `setValues` respectively.
After you've set all parameters. You can begin the optimization by calling the member function optimize which returns an array of arrays carrying the calculated values. If  `printEnable` equals 1 when creating the instance results will also be printed to `stdin`.