import time
import numpy as np
from scipy.optimize import minimize


func = lambda par: 1/(par[0]*par[1]*np.log((1.2-5)/(2.8-5)*(par[0]*0.0083+2.8-5)/(par[0]*0.0083+1.2-5)))

def toReadable(val):
    readable = {
        4:  "T",
        3:  "G",
        2:  "M",
        1:  "k",
        0:  "",
        -1: "m",
        -2: "u",
        -3: "n",
        -4: "p",
        -5: "f",
        -6: "a"
    }
    exp = np.log10(val) + 0.0000000001
    prefixIdx = int(np.floor(exp / 3))
    prefix = readable.get(prefixIdx, "Unknown")
    if(prefix == "Unknown"): return [val, ""]
    return [val*(10**(-3*prefixIdx)), prefix]

def stdVals(Int, tol = 10):

    switcher = {
        1:  96,
        2:  48,
        5:  24,
        10: 12,
        20: 6  
    }

    N = switcher.get(tol, "Invalid value.")
    if(N == "Invalid value."): return []

    if(tol <= 2): sigFig = 3
    else: sigFig = 2

    e = np.arange(0, N, 1)/N
    
    min_log10 = np.log10(Int[0])
    max_log10 = np.log10(Int[1])
    
    nMin = np.argmax(e - min_log10 % 1 > 0) + N*int(min_log10)
    nMax = np.argmax(e - max_log10 % 1 < 0) + N*int(max_log10)

    vals = np.array([float("%.{}g".format(sigFig) % val) for val in 10**(np.arange(nMin, nMax, 1)/N)])

    return vals

class Optimize:
    def __init__(self, values = [], yTarget = 0, tol = 10, epsilon = 0, units = [], printEnable = 1, sig = 3):
        self.values = values
        self.yTarget = yTarget
        self.tol = tol

        if(epsilon == 0): self.epsilon = tol/100*self.yTarget
        else: self.epsilon = epsilon
        
        self.units = units
        self.unitEnable = 0
        self.printEnable = printEnable
        self.sig = sig
        self.result = []

    def cycleAll(self, vals, prev = []):
        if(len(vals) > 1):
            for i in vals[0]:
                prev.append(i)
                self.cycleAll(vals = vals[1:], prev = prev)
                prev.pop()
        else:
            for i in vals[0]:
                toPush = prev.copy()
                toPush.append(i)
                temp = func(toPush)
                if(abs(self.yTarget-temp) < self.epsilon):
                    toPush.append(temp)
                    self.result.append(toPush)
        return

    def setyTarget(self, yTarget):
        self.yTarget = yTarget
        
    def setValues(self, values):
        self.values = values

    def show(self):
        if(len(self.values) + 1 == len(self.units)): self.unitEnable = 1
        #sort_order = self.result
        if(self.printEnable and self.unitEnable):
            for a in self.result:
                temp = ""
                for x, unit in zip(a, self.units):
                    xr = toReadable(x)
                    temp += "{} = ".format(unit[0]) + ("%.{}g".format(self.sig) % xr[0]) + " {pre}{un}\t".format(pre = xr[1], un = unit[1]) 
                print(temp)

        elif(self.printEnable):
            for a in self.result:
                temp = ""
                counter = 0
                for x in a:
                    xr = toReadable(x)
                    temp += "x{} = ".format(counter) + ("%.{}g".format(self.sig) % xr[0]) + " {pre}\t".format(pre = xr[1])
                    counter += 1
                print(temp)

    def optimize(self):
        self.cycleAll(vals = self.values)
        if(self.printEnable): self.show()
        return self.result


resistance = stdVals([600, 10000], 5)

capacitance = stdVals([10**-10, 100*10**-9], 10)
un = [["Res", "Ohm"], ["Cap", "F"],["f", "Hz"]]

start = time.time()
optimizer = Optimize([resistance, capacitance], yTarget = 300000, tol = 10, units = un)
optimizer.optimize()
finish = time.time()
print("tTot = ", finish - start)
